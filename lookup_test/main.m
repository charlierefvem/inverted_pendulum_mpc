%%
table_index = (0:0.01:100)';
table_data = [sin(table_index) cos(table_index)];

%%
x = linspace(-1,1,10);

B = zeros(10);
B(:,1)  = 1;
B(:,2)  = x;
B(:,3)  = 1/2*(3*x.^2-1);
B(:,4)  = 1/2*(5*x.^3-3*x);
B(:,5)  = 1/8*(35*x.^4 - 30*x.^2 + 3);
B(:,6)  = 1/8*(63*x.^5 - 70*x.^3 + 15*x);
B(:,7)  = 1/16*(231*x.^6 - 315*x.^4 + 105*x.^2 - 5);
B(:,8)  = 1/16*(429*x.^7 - 693*x.^5 + 315*x.^3 - 35*x);
B(:,9)  = 1/128*(6435*x.^8 - 12012*x.^6 + 6930*x.^4 - 1260*x.^2 +35);
B(:,10) = 1/128*(12155*x.^9 - 25740*x.^7 + 18018*x.^5 - 4620*x.^3 + 315*x);

figure();
plot(B);

uvec = 10*cos(0.05:0.1:0.95)'+9*sin(0.05:0.1:0.95)';
vec = B\uvec;

%%
hf=figure(1);
set(hf,'Position',[0 0 1000 500]);
im = cell(length(out.tout),1);
for n=1:length(out.tout)
    figure(hf);
    subplot(2,1,1);
        plot(out.tout, squeeze(out.r(1,1,:)), 'red', 'DisplayName', 'Reference Trajectory');
        hold on;
        plot(out.tout(n)+(0:0.1:1)', out.r(:,1,n),'red', 'DisplayName', 'Reference trajectory (window)', 'linewidth',1);
        plot(out.tout(1:n), out.x(1:n,1), 'black', 'DisplayName', 'Actual trajectory');
        plot(out.tout(n)+(0:0.1:1)', out.y(:,1,n), 'xred', 'DisplayName', 'Optimal trajectory');
        plot([out.tout(n) out.tout(n)], [-1.1 1.1],'--black', 'DisplayName', 'Time t');
        plot(1+[out.tout(n) out.tout(n)], [-1.1 1.1],':black', 'DisplayName', 'Time t+T');
        hold off;

        title('Position');
        xlim([min(out.tout) max(out.tout)]);
        ylim([-1.1 1.1]);
        legend('Location','BestOutside');
        grid on;
    
    subplot(2,1,2);
        plot(out.tout, squeeze(out.r(1,2,:)), 'blue', 'DisplayName', 'Reference Trajectory');
        hold on;
        plot(out.tout(n)+(0:0.1:1)', out.r(:,2,n),'blue', 'DisplayName', 'Reference trajectory (window)', 'linewidth',1);
        plot(out.tout(1:n), out.x(1:n,2), 'black', 'DisplayName', 'Actual trajectory');
        plot(out.tout(n)+(0:0.1:1)', out.y(:,2,n), 'xblue', 'DisplayName', 'Optimal trajectory');
        plot([out.tout(n) out.tout(n)], [-1.1 1.1],'--black', 'DisplayName', 'Time t');
        plot(1+[out.tout(n) out.tout(n)], [-1.1 1.1],':black', 'DisplayName', 'Time t+T');
        hold off;
        
        title('Velocity');
        xlim([min(out.tout) max(out.tout)]);
        ylim([-1.1 1.1]);
        legend('Location','BestOutside');
        grid on;
    
    drawnow;
    frame = getframe(hf);
    im{n} = frame2im(frame);
end

%%
filename = 'tracking_results_continuous_legendre.gif'; % Specify the output file name
for n = 1:length(out.tout)
    [A,map] = rgb2ind(im{n},256);
    if n == 1
        imwrite(A,map,filename,'gif','LoopCount',Inf,'DelayTime',0.05);
    else
        imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',0.05);
    end
end