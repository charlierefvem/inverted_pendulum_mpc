function u = fcn(e, x, P, Ts)

u = 0;
c = cos(x(2));
s = sin(x(2));

phi = P.Ith*P.mx - P.m^2*P.r^2*c^2;

A = zeros(4,4);
B = zeros(4,1);

A(1:2,3:4) = eye(2);

A(3,1) = 0;
A(3,2) = -(P.m*P.r*(-P.Ith*c*x(4)^2 + P.b2*s*x(4) + P.g*P.m*P.r*(2*c^2-1)))/phi ...
         -(2*P.m^2*P.r^2*c*s*(-P.g*c*s*P.m^2*P.r^2 + P.Ith*s*P.m*x(4)^2*P.r + P.b2*c*P.m*x(4)*P.r  + u*P.Ith - P.Ith*P.b1*x(3) ) )/phi^2;
A(3,3) = -(P.Ith*P.b1)/phi;
A(3,4) = P.m*P.r*(P.b2*c+2*P.Ith*x(4)*s)/phi;

A(4,1) = 0;
A(4,2) = P.m*P.r*(-P.m*P.r*(2*c^2-1)*x(4)^2 + u*s + P.g*P.mx*c - P.b1*x(3)*s)/phi ...
       + 2*P.m^2*P.r^2*c*s*(P.b2*P.mx*x(4) + u*P.m*P.r*c + P.m^2*x(4)^2*P.r^2*c*s - P.b1*P.m*x(3)*P.r*c-P.g*P.m*P.mx*P.r*s)/phi^2;
A(4,3) = P.b1*P.m*P.r*c/phi;
A(4,4) = -(x(4)*2*s*c*P.m^2*P.r^2 + P.b2*P.mx)/phi;


B(3) = P.Ith/phi;
B(4) = -P.m*P.r*c/phi;

randmag = 5; % seems to rarely cause failure
% randmag = 0.1;

if abs(x(2)) > 0.1
    Q = diag([1 1 1.5 1]);
    R = 0.01;
%     krand = (rand(1,4)-0.5)*randmag;
%     krand = [0 0 1 0];
    krand = [0 0 0 0];
else
    Q = diag([1 1 1 1]);
    R = 0.1;
    krand = [0 0 0 0];
end
   

% Q = diag([1 1 1 1]);
% R = 0.01;

thresh = 0.0;

if abs(cos(x(2))) > thresh
    try
%         k = lqr(A,B,Q,R);
        k = lqrd(A,B,Q,R,Ts);
        k = k + krand;
    %     k = place(A,B,[-2-1i,-2+1i,-10,-11]);

%         u = 2*x(3)-k*e;
        u = -k*e;
    catch
            u = 0;
%         u = 2*x(3);
    end
else
%     u = 2*x(3);
    u = 0;
end