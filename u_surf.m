%%
P = get_param_struct();
xd = 0;
thd = 0;
u = 0;
Ts = 1/100;

th_range = -2*pi:pi/100:2*pi;
th_range(th_range==3*pi/2)=NaN;
th_range(th_range==pi/2)=NaN;
th_range(th_range==-pi/2)=NaN;
th_range(th_range==-3*pi/2)=NaN;

[th_grid, thd_grid] = meshgrid(th_range,linspace(-10,10,101));

u_grid = zeros(size(th_grid));
x_zero = zeros(size(th_grid));

%%
for n=1:numel(th_grid)
    x = [0
         th_grid(n)
         xd
         thd_grid(n)];
    
    c = cos(x(2));
    s = sin(x(2));

    phi = P.Ith*P.mx - P.m^2*P.r^2*c^2;

    A = zeros(4,4);
    B = zeros(4,1);

    A(1:2,3:4) = eye(2);

    A(3,1) = 0;
    A(3,2) = -(P.m*P.r*(-P.Ith*c*x(4)^2 + P.b2*s*x(4) + P.g*P.m*P.r*(2*c^2-1)))/phi ...
             -(2*P.m^2*P.r^2*c*s*(-P.g*c*s*P.m^2*P.r^2 + P.Ith*s*P.m*x(4)^2*P.r + P.b2*c*P.m*x(4)*P.r  + u*P.Ith - P.Ith*P.b1*x(3) ) )/phi^2;
    A(3,3) = -(P.Ith*P.b1)/phi;
    A(3,4) = P.m*P.r*(P.b2*c+2*P.Ith*x(4)*s)/phi;

    A(4,1) = 0;
    A(4,2) = P.m*P.r*(-P.m*P.r*(2*c^2-1)*x(4)^2 + u*s + P.g*P.mx*c - P.b1*x(3)*s)/phi ...
           + 2*P.m^2*P.r^2*c*s*(P.b2*P.mx*x(4) + u*P.m*P.r*c + P.m^2*x(4)^2*P.r^2*c*s - P.b1*P.m*x(3)*P.r*c-P.g*P.m*P.mx*P.r*s)/phi^2;
    A(4,3) = P.b1*P.m*P.r*c/phi;
    A(4,4) = -(x(4)*2*s*c*P.m^2*P.r^2 + P.b2*P.mx)/phi;


    B(3) = P.Ith/phi;
    B(4) = -P.m*P.r*c/phi;
    
    Q = diag([1 1 1.5 1]);
    R = 0.01;
    try
        k = lqrd(A,B,Q,R,Ts);
    catch
        k = NaN(4,1);
    end
    
    x_zero(n) = -k(2)/k(1)*th_grid(n) - k(3)/k(1)*thd_grid(n);
end

%%
figure(1)
[C,h] = contour(x_zero,th_grid,thd_grid);
clabel(C,h);
xlabel('x');
ylabel('\theta');
xlim([-10 10]);
yticks(-2*pi:pi/2:2*pi);
yticklabels({'-2\pi' '-3\pi/2' '-\pi' '-\pi/2' '0' '\pi/2' '\pi' '3\pi/2' '2\pi'});
zlabel('u');

% figure(2)
hold on;
plot(x_zero(51,:), th_grid(51,:),'LineWidth',2);
plot(simout.x(:,1),simout.x(:,2),'LineWidth',2);
hold off;

grid on;