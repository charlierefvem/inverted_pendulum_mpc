%%
P = get_param_struct();


%% Simulate
Ts = 1/100;

load('pend_trajectory_4');

%%
% xi = [0
%       pi/12
%       0
%       0];



xi = trajectory(1,2:5);
mode = 'CL';
simout = sim('open_loop_sim',20);

%%

simout.T = 0.5*P.mx*simout.x(:,3).^2 ...
         + 0.5*P.Ith*simout.x(:,4).^2 ...
         + P.m*P.r*cos(simout.x(:,2)).*simout.x(:,3).*simout.x(:,4);
simout.V = P.m*P.g*P.r*cos(simout.x(:,2));
simout.E = simout.T + simout.V;

%%
simout.controllability = zeros(length(simout.A),1);
for n=1:length(simout.A)
    simout.controllability(n)=rank(ctrb(simout.A(:,:,n), simout.B(:,:,n)));
end

%% Interpolate
fps = 30;

simout.t_int = (0:1/fps:max(simout.tout))';
simout.x_int = interp1(simout.tout, simout.x, simout.t_int);
simout.T_int = interp1(simout.tout, simout.T, simout.t_int);
simout.V_int = interp1(simout.tout, simout.V, simout.t_int);
simout.E_int = interp1(simout.tout, simout.E, simout.t_int);

%% Plot
figure(1)
plot(simout.t_int, [simout.T_int, simout.V_int, simout.E_int]);
legend('Kinetic','Potential','Total');

%% Animate
tic
for n = 1:3:length(simout.t_int)
    figure(2);
    fill([min(simout.x(:,1))-2 min(simout.x(:,1))-2 max(simout.x(:,1))+2 max(simout.x(:,1))+2],[-0.125 0.125 0.125 -0.125],'white','LineWidth',1)
    hold on;
    fill([-.5 -.5 .5 .5]+simout.x_int(n,1),[-0.25 0.25 0.25 -0.25],'white','LineWidth',2)
    plot([simout.x_int(n,1) simout.x_int(n,1)+P.r*sin(simout.x_int(n,2))],[0 P.r*cos(simout.x_int(n,2))],'-oblack','LineWidth',4)
    plot(simout.x_int(1:n,1)+P.r*sin(simout.x_int(1:n,2)),P.r*cos(simout.x_int(1:n,2)),':black')
    hold off;
    axis('equal');
    xlim([min(simout.x(:,1))-2 max(simout.x(:,1))+2]);
%     xlim([-2 2]);
    ylim(P.r*[-1.2 1.2]);
    title(sprintf('t=%2.2f\n T=%2.2f, V=%2.2f, E=%2.2f',...
                  simout.t_int(n),simout.T_int(n),simout.V_int(n),simout.E_int(n)));
    drawnow;
end
toc