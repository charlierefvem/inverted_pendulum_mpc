xi = [0
      eps
      0
      0];

b = 0.25;

P = get_param_struct();
P.b2 = P.b2+b;

mode = 'OL';
simout = sim('open_loop_sim',0:0.01:100);


trajectory = simout.x;
trajectory = flipud(trajectory);
trajectory = [simout.tout(end)-flipud(simout.tout) trajectory];
trajectory(:,[4,5]) = -trajectory(:,[4,5]);
trajectory(2:end,6:7) = diff(trajectory(:,4:5))./diff(trajectory(:,1));


ind = find(abs(trajectory(:,6))>1e-1,1,'first');
trajectory(:,1) = trajectory(:,1) - trajectory(ind,1);
trajectory(1:ind-1,:) = [];

figure(1);
plot(trajectory(:,1), trajectory(:,2:5));

figure(2);
plot(trajectory(:,1), trajectory(:,6:7));


%% Interpolate
fps = 30;

simout.t_int = (0:1/fps:max(trajectory(:,1)))';
simout.x_int = interp1(trajectory(:,1), trajectory(:,2:end), simout.t_int);


%% Animate
for n = 1:3:length(simout.t_int)
    figure(3);
    fill([min(simout.x(:,1))-2 min(simout.x(:,1))-2 max(simout.x(:,1))+2 max(simout.x(:,1))+2],[-0.125 0.125 0.125 -0.125],'white','LineWidth',1)
    hold on;
    fill([-.5 -.5 .5 .5]+simout.x_int(n,1),[-0.25 0.25 0.25 -0.25],'white','LineWidth',2)
    plot([simout.x_int(n,1) simout.x_int(n,1)+P.r*sin(simout.x_int(n,2))],[0 P.r*cos(simout.x_int(n,2))],'-oblack','LineWidth',4)
    plot(simout.x_int(1:n,1)+P.r*sin(simout.x_int(1:n,2)),P.r*cos(simout.x_int(1:n,2)),':black')
    hold off;
    axis('equal');
%     xlim([min(simout.x(:,1))-2 max(simout.x(:,1))+2]);
    xlim([-2 2]);
    ylim(P.r*[-1.2 1.2]);
    drawnow;
end

%% Save
try
    save(uiputfile(),'trajectory');
catch
end